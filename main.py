#!/usr/bin/env python
# -*- coding:utf-8 -*-
u'''
Wake On Lan

@copyright: TIELabo.com
@license: MIT
'''

__author__ = "masaki <tielabo@exleaf.jp>"
__status__ = "production"
__version__ = "0.0.2"
__date__    = "2013/01/31"


import os
import argparse

def wake_on_lan(mac_address, ip_address = None, port = 9):
    u'''
    MacアドレスからWakeOnLanをブロードキャスト送信
    IPアドレス、Portはオプション
    '''
    import socket, struct

    mac_address_byte = mac_address.split(':')
    # マックアドレスを16進数化
    hardware_addr = struct.pack('BBBBBB'
                                , int(mac_address_byte[0], 16)
                                , int(mac_address_byte[1], 16)
                                , int(mac_address_byte[2], 16)
                                , int(mac_address_byte[3], 16)
                                , int(mac_address_byte[4], 16)
                                , int(mac_address_byte[5], 16)
                            )
    # Magic Packet作成
    magic_packet = '\xff' * 6 + hardware_addr * 16

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # ブロードキャスト送信
        udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        udp.sendto(magic_packet, (ip_address, port))
    finally:
        udp.close()


def read_setting(target):
    u'''
    setting.iniから名称検索してみる
    '''
    import ConfigParser

    setting = ConfigParser.SafeConfigParser()
    setting.read(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'setting.ini'))
    if not setting.has_section(target):
        return None

    ret = {"mac_address": "", "ip_address": None, "port": 9}
    ret["mac_address"] = setting.get(target, 'MAC')
    if setting.has_option(target, 'IP') and len(setting.get(target, 'IP')) > 0:
        ret["ip_address"] = setting.get(target, 'IP')
    if setting.has_option(target, 'PORT') and len(setting.get(target, 'PORT')) > 0:
        ret["port"] = int(setting.get(target, 'PORT'))

    return ret



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Wake-On-Lan', add_help=True, version='%(prog)s ' + __version__)
    parser.add_argument('target', action="store", type=str, help="Target Name or MAC Address")
    results = parser.parse_args()

    hits = read_setting(results.target)
    if hits == None:
        wake_on_lan(results.target)
    else:
        wake_on_lan(hits["mac_address"], hits["ip_address"], hits["port"])

